The project demonstrate usage of akka-streams. The application downloads covid data from ourworldindata.org and calculates average deaths and newCases by Country.

### Feature

- Download covid data from ourworldindata.org
- Create a source
 - Read the source of incoming data
 - Convert the incoming data to domain model
 - GroupBy countryName
 - Calculate average of deaths & newCases by country
 

<img src="https://www.scala-lang.org/resources/img/frontpage/scala-spiral.png"  width="120" height="200"><img src="https://upload.wikimedia.org/wikipedia/en/thumb/5/5e/Akka_toolkit_logo.svg/1200px-Akka_toolkit_logo.svg.png"  width="300" height="200">
